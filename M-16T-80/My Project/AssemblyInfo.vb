﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("M-16T-80")> 
<Assembly: AssemblyDescription("Production Line Test Program, 484A Std")> 
<Assembly: AssemblyCompany("APG Cash Drawer, LLC")> 
<Assembly: AssemblyProduct("M-16T-80")> 
<Assembly: AssemblyCopyright("© 2015 APG Cash Drawer, LLC")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("4d561472-7ae1-4bec-b5da-2b8966656716")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.2.3.2")> 
<Assembly: AssemblyFileVersion("1.2.3.2")> 
