﻿Imports System.IO.Ports
Imports System.Text
Imports System.Xml
Public Class fMain
    '
#Region "  variables declaration"
    Private locX As Int32 = 20
    Private locY As Int32 = 30
    Private m_counter As Int32 = 0
    Private m_DSPollCTS As String = Nothing
    Private m_isCommOpen As Boolean = False
    Private m_isJamTestRqd As Boolean = False
    'Private m_RiEventCounter As Int32 = 0
    Private m_TestNo As Int32 = 0
    Private ports As String() = SerialPort.GetPortNames()

    Private Const HORIZ_BUTTON_SPACING As Int32 = 20
    Private Const VERT_BUTTON_SPACING As Int32 = 20
    Private Const MAX_DRAWERS As Int32 = 2500
    Private Const MAX_PROPS As Int32 = 9            '0-based: 0 to 9 = 10 properties
    Private Const NODE_START As String = "CashDrawer"
    Private CashDrawersR(,) As String           'data storage array from Xml

    Private BUTTONFONT As Font = New Font("Verdana", 14.0!, FontStyle.Regular, GraphicsUnit.Point)
    Private BUTTONSIZE As Size = New Size(150, 40)
    Private LABELFONT As Font = New Font("Verdana", 18.0!, FontStyle.Bold, GraphicsUnit.Point)
    Private TEXTFONT As Font = New Font("Verdana", 11.0!, FontStyle.Regular, GraphicsUnit.Point)
    Private TEXTINPUTFONT As Font = New Font("Verdana", 18.0!, FontStyle.Italic, GraphicsUnit.Point)
    Private TEXTINPUTSIZE As Size = New Size(450, 50)

    Private m_cdPartNo As String = Nothing
    Private m_DKChar As Int32 = 0
    Private m_dwrNum As Int32 = 0
    Private m_frmTitle As String = Nothing
    Private m_numDKChars As Int32 = 0
    Private m_numDrawers As Int32 = 0
    Private m_read_Xml As String = Nothing
    Private m_UsrMsg As String = Nothing
    Private m_write_Xml As String = Nothing
    Private currentDateTime As String = DateTime.Now.ToString("s")              '2013-08-14T16:47:55
    Private m_currentDate As String = currentDateTime.Substring(5, 2) + _
                                        currentDateTime.Substring(8, 2) + _
                                        currentDateTime.Substring(0, 4)         'MMDDYYYY
    Private m_currentTime As String = currentDateTime.Substring(11, 8)          ''HH:MM:SS' 24-hr clock

    ' controls declaration
    Private btnExit As Button
    Private btnJamTest As Button
    Private btnNo As Button
    Private btnOk As Button
    Private btnOk1 As Button
    Private btnOk2 As Button
    Private btnOk3 As Button
    Private btnOk4 As Button
    Private btnOk5 As Button
    Private btnPrintLabel As Button
    Private btnYes As Button
    Private _lblClockTime As Label
    Private _lblClockDate As Label
    Private _lblDisconnect As Label
    Private _lblUm(3) As Label
    Private _tbox As TextBox
    Private m_TimerConnectDetect As Timer
    Private m_TimerKillApp As Timer
    Private m_ChargingCycle As Timer
    Private m_TimerTitleClock As Timer
    Private m_TimerWaitForDrawerClose As Timer

    ' assign path and filename for Xml
    Private Function pathfn_484A_STD_DrawersXML() As String
        Return Application.StartupPath.ToString & "\484A_StdDrawersXML.xml"
    End Function

    Private Enum drawerType
        StdProduct = 0
        oemCTSHi = 1
        oemCTSLow = 2
        oemRIHi = 3
        oemRILow = 4
    End Enum
    Private m_DrawerType As drawerType
#End Region

#Region "  form load and primary controls"
    Private Sub fMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Setup_fMain()
    End Sub
    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        End
    End Sub
#End Region

#Region "  setup fMain"
    Private Sub Setup_fMain()
        With Me
            .BackColor = Color.CadetBlue
            .CancelButton = btnExit
            .FormBorderStyle = FormBorderStyle.Fixed3D
            .Location = New Point(10, 5)
            .MaximizeBox = False
            .MinimizeBox = False
            .Size = New Size(1050, 925)
            .Text = My.Application.Info.Description.ToString & " [" + _
                     My.Application.Info.ProductName.ToString & ": v" + _
                     My.Application.Info.Version.ToString + "] " + _
                     My.Application.Info.CompanyName & "   " + _
                     CType(DateTime.Now, String)
        End With
        m_frmTitle = My.Application.Info.Description.ToString & " [" + _
                     My.Application.Info.ProductName.ToString & ": v" + _
                     My.Application.Info.Version.ToString + "] " + _
                     My.Application.Info.CompanyName

        For i As Int32 = 0 To 3
            _lblUm(i) = New Label
            Me.Controls.Add(_lblUm(i))
            With _lblUm(i)
                .Font = LABELFONT
                .ForeColor = Color.White
                .Location = New Point(locX, locY)
                locY += 40
                .Size = New Size(Me.Width - 30, 40)
                .Text = "label " + CStr(i) + " presented here. NOW IS THE TIME FOR ALL GOOD MEN TO COME TO THE AID OF HIS COUNTRY"
            End With
        Next

        btnExit = New Button
        Me.Controls.Add(btnExit)
        AddHandler btnExit.Click, AddressOf btnExit_Click
        With btnExit
            .BackColor = Color.FromKnownColor(KnownColor.Control)
            .Enabled = True
            .Font = BUTTONFONT
            .Location = New Point(Me.Width - 190, Me.Height - 90)
            .Size = BUTTONSIZE
            .TabStop = False
            .Text = "Exit"
        End With

        With txtPN
            .Font = TEXTINPUTFONT
            .Location = New Point(locX, locY)
            .Multiline = False
            .Size = TEXTINPUTSIZE
            .Text = "Scan drawer pn on the box label"
            '.Text = "T484A-BL1616"
            .SelectAll()
            .Visible = False
        End With

        With pctGreenLEDon
            .Visible = False
        End With

        With pctLockDrawer
            .Visible = False
        End With

        With pctStdStart
            .Visible = False
        End With

        With pctStdFinal
            .Visible = False
        End With

        Call Setup_Timer_Instances()
        Call Get_Drawer_Count_From_Xml()
        Call Load_Data_Xml()
        'Call Display_Xml_Data()
        'Controls.Remove(_tbox)
        Call Scan_Drawer_Part_Number()
    End Sub
    Private Sub Setup_Timer_Instances()
        m_TimerTitleClock = New Timer
        With m_TimerTitleClock
            .Interval = 950
            .Enabled = True
        End With
        AddHandler m_TimerTitleClock.Tick, AddressOf m_TimerTitleClockTick
    End Sub
    Private Sub m_TimerTitleClockTick(ByVal sender As Object, ByVal e As EventArgs)
        With Me
            .Text = m_frmTitle + "   " + DateAndTime.Now.ToString
        End With
    End Sub
#End Region

#Region "  kill app timer"
    Private Sub Kill_App(ByVal usermsg0 As String, ByVal usermsg1 As String, _
                         ByVal usermsg2 As String, ByVal usermsg3 As String)

        With m_Comm
            .Close()
        End With

        _lblUm(0).Text = usermsg0
        _lblUm(1).Text = usermsg1
        _lblUm(2).Text = usermsg2
        _lblUm(3).Text = usermsg3

        With m_TimerTitleClock
            .Enabled = False
        End With

        m_TimerKillApp = New Timer
        With m_TimerKillApp
            .Interval = 1000
            .Enabled = True
        End With
        AddHandler m_TimerKillApp.Tick, AddressOf m_TimerKillAppTick
    End Sub

    Private Sub m_TimerKillAppTick(ByVal sender As Object, ByVal e As EventArgs)
        Select Case m_counter
            Case 0
                End
            Case 1
                _lblUm(3).Text = "Program closing in " & m_counter & " second . . ."
                m_counter -= 1
            Case Else
                _lblUm(3).Text = "Program closing in " & m_counter & " seconds . . ."
                m_counter -= 1
        End Select
    End Sub
#End Region

#Region "  load Xml, get drawer count, assign attributes to variables, optionally display Xml data"
    ' load drawer count from the Xml
    Private Sub Get_Drawer_Count_From_Xml()
        Dim xmlReader As New XmlTextReader(pathfn_484A_STD_DrawersXML)
        m_numDrawers = 0
        xmlReader.MoveToContent()
        Do While xmlReader.Read
            If ((xmlReader.NodeType = XmlNodeType.Element) And _
                (xmlReader.Name = NODE_START)) Then m_numDrawers += 1
        Loop
        xmlReader.Close()
        _lblUm(0).Text = CStr(m_numDrawers) + " drawers defined in the XML"
        ReDim CashDrawersR((m_numDrawers + 1), MAX_PROPS)
    End Sub

    ' load Xml data elements and assign to variables
    Private Sub Load_Data_Xml()
        Dim j As Int32 = 0
        Dim xmlReader As New XmlTextReader(pathfn_484A_STD_DrawersXML)

        m_dwrNum = 0
        For j = 0 To MAX_PROPS
            CashDrawersR(m_dwrNum, j) = j.ToString + ":00-DUMMY-00"
        Next

        xmlReader.MoveToContent()
        Do While (xmlReader.Read())
            Select Case xmlReader.NodeType
                Case XmlNodeType.Element
                    If (xmlReader.Name = "CashDrawer") Then
                        m_dwrNum += 1
                        j = 0
                    End If
                Case XmlNodeType.Text
                    CashDrawersR(m_dwrNum, j) = xmlReader.Value
                    j += 1
                Case XmlNodeType.EndElement
                    'do nothing
            End Select
        Loop
        xmlReader.Close()
    End Sub

    ' optional: display loaded cash drawer data
    Private Sub Display_Xml_Data()
        _tbox = New TextBox
        Controls.Add(_tbox)
        With _tbox
            .BackColor = Color.WhiteSmoke
            .Clear()
            .Font = BUTTONFONT
            .Location = New Point(20, locY)
            .Multiline = True
            .ReadOnly = True
            .Size = New Size(400, 375)
            .Visible = True
        End With

        _tbox.AppendText("no. drawers in xml: " + m_numDrawers.ToString + Environment.NewLine)
        For i As Int32 = 0 To m_numDrawers
            '_tbox.AppendText(CStr(i) + ":" + CashDrawersR(i, 0) + Environment.NewLine)
            For j As Int32 = 0 To MAX_PROPS
                _tbox.AppendText(CStr(i) + ":" + CashDrawersR(i, j) + Environment.NewLine)
            Next
        Next
    End Sub
#End Region

#Region "  define and open comm port"
    Private Sub Open_Comm_Port()
        Dim port As String = ""

        Try
            For Each port In ports
                'MessageBox.Show(port)
            Next port

            If (port = "") Then
                'do nothing
            Else
                With m_Comm
                    .BaudRate = 9600
                    .DataBits = 8
                    .DtrEnable = True
                    .Handshake = Handshake.None
                    .Parity = Parity.None
                    .PortName = port
                    .RtsEnable = True
                    .StopBits = StopBits.One
                End With
            End If
        Catch ex As Exception

        End Try

        Try
            m_Comm.Open()
            m_isCommOpen = m_Comm.IsOpen
        Catch ex As Exception
            'm_isCommOpen = False
            ''MessageBox.Show(ex.Message.ToString)
            'Dim msg1 As String = "Unable to open the defined COM port. Confirm equipment settings"
            'Dim msg2 As String = "and restart test. Consult with production supervision or"
            'Dim msg3 As String = "engineering as needed for assistance"
            'Dim msg4 As String = ""
            'm_counter = 5
            'Kill_App(msg1, msg2, msg3, msg4)
        End Try

        If (m_isCommOpen) Then
            With tmrPollCTS
                .Interval = 250
                .Enabled = True
            End With
        End If
    End Sub
#End Region

#Region "  scan cash drawer part number from label"
    Private Sub Scan_Drawer_Part_Number()
        _lblUm(0).Text = "M-16T-80 Production Line Test Program"
        _lblUm(1).Text = "Test for Standard 484A Drawers"
        _lblUm(2).Text = "Scan the cash drawer part number from the box label"
        _lblUm(3).Text = ""

        With txtPN
            .Location = New Point(locX, locY)
            .Visible = True
            .SelectAll()
            .Focus()
        End With
    End Sub
    Private Sub txtPN_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPN.KeyPress
        Dim blnDrawerFoundInXml As Boolean = False
        Dim scanned_drawerpartno As String = Nothing
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            scanned_drawerpartno = txtPN.Text.ToUpper
            If (String.IsNullOrEmpty(txtPN.Text.ToUpper)) Then
                Dim msg1 As String = "Data entry error - no information entered."
                Dim msg2 As String = "Application closing. Please re-start from the Windows Desktop."
                Dim msg3 As String = ""
                Dim msg4 As String = ""
                m_counter = 5
                Call Kill_App(msg1, msg2, msg3, msg4)
            Else
                'MessageBox.Show(scanned_drawerpartno)
                For i As Int32 = 1 To m_numDrawers
                    If (scanned_drawerpartno.Substring(0, 9) = CashDrawersR(i, 0)) Then
                        blnDrawerFoundInXml = True
                        m_dwrNum = i
                        Call Get_DK_Details()
                        If ((CashDrawersR(i, 0).Substring(0, 1) = "J") Or _
                            (CashDrawersR(i, 0).Substring(0, 1) = "T")) Then
                            m_isJamTestRqd = True
                        End If
                        'MessageBox.Show("drawer found in xml, " + blnDrawerFoundInXml.ToString)
                    Else
                        Dim testInput As userInput = New userInput
                        If (testInput.confirm_drawerpartnumber(scanned_drawerpartno)) Then
                            'QM- or QM1- being built
                            blnDrawerFoundInXml = True
                        End If
                        m_dwrNum = 1
                        m_DKChar = 7
                        m_numDKChars = 1
                    End If
                Next
            End If
            If ((scanned_drawerpartno.Contains("S22R")) Or _
                (scanned_drawerpartno.Contains("S22")) Or _
                (scanned_drawerpartno.Contains("-S5"))) Then
                blnDrawerFoundInXml = False
            End If
            If (blnDrawerFoundInXml) Then
                CashDrawersR(m_dwrNum, 0) = scanned_drawerpartno
                m_DrawerType = drawerType.StdProduct
                txtPN.Visible = False
                Call Set_And_Connect()
            Else
                Dim msg1 As String = "This cash drawer is undefined, mismatched, or bad data"
                Dim msg2 As String = "was entered. Please restart the program and try again."
                Dim msg3 As String = "Notify production supervision for assistance."
                Dim msg4 As String = ""
                m_counter = 5
                Call Kill_App(msg1, msg2, msg3, msg4)
            End If
        End If
    End Sub
#End Region

#Region "  set and connect - dip switches"
    Private Sub Set_And_Connect()
        Dim port As String = ""
        Call Open_Comm_Port()

        For Each port In ports
            'MessageBox.Show(port)
        Next port

        If (m_Comm.IsOpen) Then
            _lblUm(1).Text = "Drawer Found. Connect the cash drawer's power adapter - green LED ON"
            _lblUm(2).Text = "Set the dip switches as shown.  Close the drawer.  (" + port + ")"
            _lblUm(3).Text = "Connect the drawer to the computer. Click 'Ok' to continue . . ."

            Select Case m_DrawerType
                Case drawerType.StdProduct

                    With pctGreenLEDon
                        .Location = New Point(locX, locY)
                        .Visible = True
                    End With

                    locX = pctGreenLEDon.Location.X + pctGreenLEDon.Width + HORIZ_BUTTON_SPACING
                    locY = pctGreenLEDon.Location.Y

                    With pctStdStart
                        .Location = New Point(locX, locY)
                        .Size = New Size(600, 291)
                        .Visible = True
                    End With

                    btnOk = New Button
                    Me.Controls.Add(btnOk)
                    AddHandler btnOk.Click, AddressOf btnOk_Click
                    With btnOk
                        .BackColor = Color.FromKnownColor(KnownColor.Control)
                        .Enabled = True
                        .Font = BUTTONFONT
                        .Location = New Point(btnExit.Location.X - HORIZ_BUTTON_SPACING - btnExit.Width, btnExit.Location.Y)
                        .Size = BUTTONSIZE
                        .Text = "Ok"
                        .Focus()
                    End With
            End Select
        Else
            Dim msg1 As String = "Unable to open the defined COM port. Confirm equipment settings"
            Dim msg2 As String = "and restart test. Consult with production supervision or"
            Dim msg3 As String = "engineering as needed for assistance"
            Dim msg4 As String = ""
            m_counter = 5
            Call Kill_App(msg1, msg2, msg3, msg4)
        End If
    End Sub
    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.Controls.Remove(btnOk)
        tmrPollCTS.Enabled = False
        Threading.Thread.Sleep(750)
        If ((CashDrawersR(m_dwrNum, 5) = "Enabled") And (m_Comm.DsrHolding)) Then
            If ((CashDrawersR(m_dwrNum, 6) = "Enabled") And (m_Comm.CDHolding)) Then
                If (m_DSPollCTS = "CLOSED") Then
                    _lblUm(1).Text = ""
                    _lblUm(2).Text = "Open the cash drawer with the key."
                    _lblUm(3).Text = "Click 'Ok' to continue . .  ."
                    tmrPollCTS.Enabled = True
                    Call Confirm_Drawer_Status_Opened()
                Else
                    pctStdStart.Visible = False
                    pctGreenLEDon.Visible = False
                    Dim msg0 As String = "Drawer status error - CTS not LOW"
                    Dim msg1 As String = "Confirm drawer wiring, position, and dip switches"
                    Dim msg2 As String = "Program closing. Re-start test when you are ready to continue."
                    Dim msg3 As String = ""
                    m_counter = 5
                    Kill_App(msg0, msg1, msg2, msg3)
                End If
            Else
                pctStdStart.Visible = False
                pctGreenLEDon.Visible = False
                Dim msg0 As String = "Wiring error - data carrier detect not holding"
                Dim msg1 As String = "Confirm drawer cabling and dip switches"
                Dim msg2 As String = "Program closing. Re-start test when you are ready to continue."
                Dim msg3 As String = ""
                m_counter = 5
                Kill_App(msg0, msg1, msg2, msg3)
            End If
        Else
            pctStdStart.Visible = False
            pctGreenLEDon.Visible = False
            Dim msg0 As String = "Wiring error - data set ready not holding"
            Dim msg1 As String = "Confirm drawer cabling and dip switches"
            Dim msg2 As String = "Program closing. Re-start test when you are ready to continue."
            Dim msg3 As String = ""
            m_counter = 5
            Kill_App(msg0, msg1, msg2, msg3)
        End If
    End Sub
#End Region

#Region "  drawer status"
    Private Sub tmrPollCTS_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrPollCTS.Tick
        Select Case m_DrawerType
            Case drawerType.StdProduct
                If (m_Comm.CtsHolding) Then
                    m_DSPollCTS = "CLOSED"
                Else
                    m_DSPollCTS = "OPEN"
                End If
        End Select
    End Sub

    Private Sub Confirm_Drawer_Status_Opened()
        btnOk2 = New Button
        Me.Controls.Add(btnOk2)
        AddHandler btnOk2.Click, AddressOf btnOk2_Click
        With btnOk2
            .BackColor = Color.FromKnownColor(KnownColor.Control)
            .Enabled = True
            .Font = BUTTONFONT
            .Location = New Point(btnExit.Location.X - HORIZ_BUTTON_SPACING - btnExit.Width, btnExit.Location.Y)
            .Size = BUTTONSIZE
            .Text = "Ok"
            .Focus()
        End With
    End Sub

    Private Sub btnOk2_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.Controls.Remove(btnOk2)
        tmrPollCTS.Enabled = False

        If (m_DSPollCTS = "OPEN") Then
            'CTS confirmed: drawer opened by user
            _lblUm(1).Text = ""
            _lblUm(2).Text = "Close the cash drawer."
            _lblUm(3).Text = "Click 'Ok' to continue . .  ."
            tmrPollCTS.Enabled = True
            Call Confirm_Drawer_Status_Closed()
        Else
            btnExit.Focus()
            pctStdStart.Visible = False
            pctGreenLEDon.Visible = False
            Dim msg0 As String = "Drawer status fault - incorrect drawer status detected (" + _
                                 m_DSPollCTS + ")"
            Dim msg1 As String = "Confirm drawer wiring and dip switches"
            Dim msg2 As String = "Program closing. Re-start test when you are ready to continue."
            Dim msg3 As String = ""
            m_counter = 5
            Kill_App(msg0, msg1, msg2, msg3)
        End If
    End Sub

    Private Sub Confirm_Drawer_Status_Closed()
        btnOk3 = New Button
        Me.Controls.Add(btnOk3)
        AddHandler btnOk3.Click, AddressOf btnOk3_Click
        With btnOk3
            .BackColor = Color.FromKnownColor(KnownColor.Control)
            .Enabled = True
            .Font = BUTTONFONT
            .Location = New Point(btnExit.Location.X - HORIZ_BUTTON_SPACING - btnExit.Width, btnExit.Location.Y)
            .Size = BUTTONSIZE
            .Text = "Ok"
            .Focus()
        End With
    End Sub
    Private Sub btnOk3_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.Controls.Remove(btnOk3)
        tmrPollCTS.Enabled = False

        If (m_DSPollCTS = "CLOSED") Then
            If (m_DrawerType = drawerType.StdProduct) Then
                Call Set_Dips_To_Final()
            End If
        Else
            btnExit.Focus()
            pctStdStart.Visible = False
            pctGreenLEDon.Visible = False
            Dim msg0 As String = "Drawer status fault - incorrect drawer status detected (" + _
                                 m_DSPollCTS + ")"
            Dim msg1 As String = "Confirm drawer wiring and dip switches"
            Dim msg2 As String = "Program closing. Re-start test when you are ready to continue."
            Dim msg3 As String = ""
            m_counter = 5
            Kill_App(msg0, msg1, msg2, msg3)
        End If
    End Sub
#End Region

#Region "  set dips to final"
    Private Sub Set_Dips_To_Final()

        _lblUm(1).Text = "Set the dip switches to final as shown."
        _lblUm(2).Text = "Click 'Ok' to continue . . ."
        _lblUm(3).Text = ""

        With pctStdStart
            .Visible = False
        End With

        With pctStdFinal
            .Location = New Point(pctStdStart.Location.X, pctStdStart.Location.Y)
            .Size = New Size(pctStdStart.Width, pctStdStart.Height)
            .Visible = True
        End With

        btnOk4 = New Button
        Me.Controls.Add(btnOk4)
        AddHandler btnOk4.Click, AddressOf btnOk4_Click
        With btnOk4
            .BackColor = Color.FromKnownColor(KnownColor.Control)
            .Enabled = True
            .Font = BUTTONFONT
            .Location = New Point(btnExit.Location.X - HORIZ_BUTTON_SPACING - btnExit.Width, btnExit.Location.Y)
            .Size = BUTTONSIZE
            .Text = "Ok"
            .Focus()
        End With

    End Sub

    Private Sub btnOk4_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.Controls.Remove(btnOk4)

        Call Charging_Drawer_With_Power_Adapter()
        'MessageBox.Show("Charging_Drawer_With_Power_Adapter")
    End Sub
#End Region

#Region "  timer - charging drawer with power adapter"
    Private Sub Charging_Drawer_With_Power_Adapter()
        m_ChargingCycle = New Timer
        With m_ChargingCycle
            .Interval = 1000
            .Enabled = True
        End With
        AddHandler m_ChargingCycle.Tick, AddressOf m_ChargingCycleTick
        m_counter = 4
    End Sub
    Private Sub m_ChargingCycleTick(ByVal sender As Object, ByVal e As EventArgs)
        Select Case m_counter
            Case -1
                MessageBox.Show("here")
            Case 0
                If (m_TestNo = 0) Then
                    If (m_DrawerType = drawerType.StdProduct) Then
                        Call Drawer_Kick()
                        m_TestNo += 1
                        _lblUm(1).Text = "Drawer kick command #" + m_TestNo.ToString + " of 3 sent."
                        _lblUm(2).Text = "Please close the cash drawer . . ."
                        _lblUm(3).Text = ""
                        m_counter = 4
                    End If
                Else
                    If (m_DrawerType = drawerType.StdProduct) Then
                        If (m_Comm.CtsHolding) Then
                            Call Drawer_Kick()
                            m_TestNo += 1
                            _lblUm(1).Text = "Drawer kick command #" + m_TestNo.ToString + " of 3 sent."
                            _lblUm(2).Text = "Please close the cash drawer . . ."
                            _lblUm(3).Text = ""
                            m_counter = 4

                            If (m_TestNo > 2) Then
                                m_ChargingCycle.Enabled = False
                                If (m_isJamTestRqd) Then
                                    Call Jam_Test()
                                Else
                                    Call Test_Success()
                                End If
                            Else
                                m_counter = 3
                            End If
                        Else
                            m_ChargingCycle.Enabled = False
                            Dim msg0 As String = "Drawer status fault - incorrect drawer status detected"
                            Dim msg1 As String = "Confirm drawer wiring and dip switches."
                            Dim msg2 As String = "Program closing. Re-start test when you are ready to continue."
                            Dim msg3 As String = ""
                            m_counter = 5
                            Kill_App(msg0, msg1, msg2, msg3)
                        End If
                    Else
                        m_ChargingCycle.Enabled = False
                        Dim msg0 As String = "Unsupported Drawer type"
                        Dim msg1 As String = "Confirm drawer type, wiring and dip switches."
                        Dim msg2 As String = "Program closing. Re-start test when you are ready to continue."
                        Dim msg3 As String = ""
                        m_counter = 5
                        Kill_App(msg0, msg1, msg2, msg3)
                    End If
                End If
            Case 1
                _lblUm(3).Text = "Please wait " & m_counter & " second for board charge . . ."
                m_counter -= 1
            Case Else
                _lblUm(3).Text = "Please wait " & m_counter & " seconds for board charge . . ."
                m_counter -= 1
        End Select
    End Sub
#End Region

#Region "  drawer kick"
    Private Sub Drawer_Kick()
        With m_Comm
            If (m_numDKChars = 1) Then .Write(Chr(m_DKChar))
            If (m_numDKChars = 2) Then .Write(Chr(m_DKChar) + Chr(m_DKChar))
        End With
    End Sub
    Private Sub Get_DK_Details()
        Dim i As Int32 = 0
        Dim separatorFound As Int32 = 0
        Dim temp As String = Nothing

        Do Until (separatorFound = 1)
            If (CashDrawersR(m_dwrNum, 9).Substring(i, 1) = "|") Then
                separatorFound = 1
            End If
            i += 1
        Loop

        separatorFound = 0
        Do Until (separatorFound = 1)
            temp += CashDrawersR(m_dwrNum, 9).Substring(i, 1)
            i += 1
            If (CashDrawersR(m_dwrNum, 9).Substring(i, 1) = "|") Then
                separatorFound = 1
            End If
        Loop
        m_DKChar = CType(temp, Int32)

        i += 1
        separatorFound = 0
        temp = ""
        Do Until (separatorFound = 1)
            temp += CashDrawersR(m_dwrNum, 9).Substring(i, 1)
            i += 1
            If (CashDrawersR(m_dwrNum, 9).Substring(i, 1) = "|") Then
                separatorFound = 1
            End If
        Loop
        m_numDKChars = CType(temp, Int32)

    End Sub
#End Region

#Region "  jam test"
    Private Sub Jam_Test()
        Me.Controls.Remove(btnOk4)

        _lblUm(1).Text = ""
        _lblUm(2).Text = "Close the drawer, lock it and remove the key"
        _lblUm(3).Text = "Click 'Jam Test' to continue . . ."

        With pctGreenLEDon
            '.Visible = False
        End With

        With pctStdFinal
            .Visible = False
        End With

        With pctLockDrawer
            .Location = New Point(locX, locY)
            .Visible = True
        End With

        btnJamTest = New Button
        Me.Controls.Add(btnJamTest)
        AddHandler btnJamTest.Click, AddressOf btnJamTest_Click
        With btnJamTest
            .BackColor = Color.FromKnownColor(KnownColor.Control)
            .Enabled = True
            .Font = BUTTONFONT
            .Location = New Point(btnExit.Location.X - HORIZ_BUTTON_SPACING - btnExit.Width, btnExit.Location.Y)
            .Size = BUTTONSIZE
            .Text = "Jam Test"
            .Visible = True
        End With

        btnJamTest.Focus()

    End Sub

    Private Sub btnJamTest_Click(ByVal sender As Object, ByVal e As EventArgs)
        btnJamTest.Enabled = False

        _lblUm(1).Text = ""
        _lblUm(1).Refresh()
        _lblUm(2).Text = "Jam Test in Progress . . ."
        _lblUm(2).Refresh()
        _lblUm(3).Text = ""
        _lblUm(3).Refresh()

        For i As Int32 = 1 To 3
            Threading.Thread.Sleep(4000)
            _lblUm(3).Text = "Test #" + i.ToString + " of 3 sent to drawer"
            _lblUm(3).Refresh()
            Call Drawer_Kick()
        Next

        Me.Controls.Remove(btnJamTest)

        If (m_DrawerType = drawerType.StdProduct) Then
            pctLockDrawer.Visible = False
            Call Setup_Print_Label()
        Else
            btnExit.Focus()
            Dim msg0 As String = "## TEST FAILURE ##"
            Dim msg1 As String = "The drawer opened during the Jam Test"
            Dim msg2 As String = "Correct the condition. Contact supervision for support"
            Dim msg3 As String = ""
            m_counter = 5
            Kill_App(msg0, msg1, msg2, msg3)
        End If
    End Sub
#End Region

#Region "  print label"
    Private Sub Setup_Print_Label()
        _lblUm(1).Text = ""
        _lblUm(2).Text = "Click 'Print Label' to continue . . ."
        _lblUm(3).Text = ""

        btnPrintLabel = New Button
        Me.Controls.Add(btnPrintLabel)
        AddHandler btnPrintLabel.Click, AddressOf btnPrintLabel_Click
        With btnPrintLabel
            .BackColor = Color.FromKnownColor(KnownColor.Control)
            .Enabled = True
            .Font = BUTTONFONT
            .Location = New Point(btnExit.Location.X - HORIZ_BUTTON_SPACING - btnExit.Width, btnExit.Location.Y)
            .Size = BUTTONSIZE
            .TabStop = False
            .Text = "Print Label"
        End With
        btnPrintLabel.Focus()
    End Sub
    Private Sub btnPrintLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Controls.Remove(btnPrintLabel)
        btnPrintLabel.Enabled = False
        Dim printControl = New Printing.StandardPrintController
        prtLabel.PrintController = printControl
        prtLabel.Print()
    End Sub
    Private Sub prtLabel_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles prtLabel.PrintPage
        Dim xPos As Int32 = 4
        Dim yPos As Int32 = 8
        Dim BOLD_FONT As New Font("Verdana", 7.0!, FontStyle.Bold, GraphicsUnit.Point)
        Dim BOLD_SMALL_FONT As New Font("Verdana", 6.0!, FontStyle.Bold, GraphicsUnit.Point)
        Dim REG_FONT As New Font("Verdana", 7.0!, FontStyle.Regular, GraphicsUnit.Point)

        Dim msg0 As String = Nothing
        Dim msg1 As String = Nothing
        Dim msg2 As String = Nothing
        Dim msg3 As String = Nothing
        Dim msg4 As String = Nothing
        Dim todaysdate As String = String.Format("{0:MM/dd/yyyy}", DateTime.Now)

        msg0 = "RS232 484A"
        msg1 = CashDrawersR(m_dwrNum, 0)
        msg2 = "Drawer Status: " + CashDrawersR(m_dwrNum, 3)
        msg3 = "Status Phase: " + CashDrawersR(m_dwrNum, 4)
        msg4 = "www.cashdrawer.com"

        e.Graphics.DrawString(msg0, BOLD_FONT, Brushes.Black, xPos, yPos)
        yPos += 12
        If (msg1.Length > 16) Then
            e.Graphics.DrawString(msg1, BOLD_SMALL_FONT, Brushes.Black, xPos, yPos)
        Else
            e.Graphics.DrawString(msg1, BOLD_FONT, Brushes.Black, xPos, yPos)
        End If
        yPos += 12
        e.Graphics.DrawString(msg2, REG_FONT, Brushes.Black, xPos, yPos)
        yPos += 12
        e.Graphics.DrawString(msg3, REG_FONT, Brushes.Black, xPos, yPos)
        yPos += 12
        e.Graphics.DrawString(msg4, REG_FONT, Brushes.Black, xPos, yPos)
        yPos += 12
        e.Graphics.DrawString("Mfg: " + todaysdate, REG_FONT, Brushes.Black, xPos, yPos)
        e.HasMorePages = False

        msg1 = "Testing is complete and the drawer has passed successfully."
        msg2 = "Apply the label printed here to the drawer per instructions."
        msg3 = "Prepare the next drawer for test. Re-launch this program from"
        msg4 = "the Windows Desktop when ready to test the next drawer."
        m_counter = 5
        btnExit.Focus()
        Call Kill_App(msg1, msg2, msg3, msg4)
    End Sub
#End Region

#Region " test success"
    Private Sub Test_Success()
        btnExit.Focus()

        With m_Comm
            .Close()
        End With

        Dim msg0 As String = "** Success **"
        Dim msg1 As String = "Test is complete. Disconnect the cash drawer."
        Dim msg2 As String = "Do not change the dip switch settings. Ship as is"
        Dim msg3 As String = ""
        m_counter = 5
        Kill_App(msg0, msg1, msg2, msg3)
    End Sub
#End Region

#Region "  revision history"
    '
    '   v1.1.3.2    6/4/2015 PSG Initial Release
    '   v1.2.3.1    6/5/2015 PSG Corrected QM numbering support
    '   v1.2.3.2    6/5/2015 PSG Parse drawer pn for S22, S22R, or -S5 and reject if found.
    '
    '
    '
#End Region

End Class
