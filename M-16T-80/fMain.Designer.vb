﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(fMain))
        Me.txtPN = New System.Windows.Forms.TextBox
        Me.pctGreenLEDon = New System.Windows.Forms.PictureBox
        Me.pctLockDrawer = New System.Windows.Forms.PictureBox
        Me.pctStdStart = New System.Windows.Forms.PictureBox
        Me.pctStdFinal = New System.Windows.Forms.PictureBox
        Me.m_Comm = New System.IO.Ports.SerialPort(Me.components)
        Me.prtLabel = New System.Drawing.Printing.PrintDocument
        Me.tmrPollCTS = New System.Windows.Forms.Timer(Me.components)
        CType(Me.pctGreenLEDon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctLockDrawer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctStdStart, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pctStdFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtPN
        '
        Me.txtPN.Location = New System.Drawing.Point(12, 12)
        Me.txtPN.Name = "txtPN"
        Me.txtPN.Size = New System.Drawing.Size(100, 20)
        Me.txtPN.TabIndex = 1
        '
        'pctGreenLEDon
        '
        Me.pctGreenLEDon.Image = CType(resources.GetObject("pctGreenLEDon.Image"), System.Drawing.Image)
        Me.pctGreenLEDon.Location = New System.Drawing.Point(12, 38)
        Me.pctGreenLEDon.Name = "pctGreenLEDon"
        Me.pctGreenLEDon.Size = New System.Drawing.Size(371, 541)
        Me.pctGreenLEDon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pctGreenLEDon.TabIndex = 4
        Me.pctGreenLEDon.TabStop = False
        Me.pctGreenLEDon.Visible = False
        '
        'pctLockDrawer
        '
        Me.pctLockDrawer.Image = CType(resources.GetObject("pctLockDrawer.Image"), System.Drawing.Image)
        Me.pctLockDrawer.Location = New System.Drawing.Point(12, 38)
        Me.pctLockDrawer.Name = "pctLockDrawer"
        Me.pctLockDrawer.Size = New System.Drawing.Size(536, 322)
        Me.pctLockDrawer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pctLockDrawer.TabIndex = 5
        Me.pctLockDrawer.TabStop = False
        Me.pctLockDrawer.Visible = False
        '
        'pctStdStart
        '
        Me.pctStdStart.Image = CType(resources.GetObject("pctStdStart.Image"), System.Drawing.Image)
        Me.pctStdStart.Location = New System.Drawing.Point(554, 38)
        Me.pctStdStart.Name = "pctStdStart"
        Me.pctStdStart.Size = New System.Drawing.Size(891, 432)
        Me.pctStdStart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pctStdStart.TabIndex = 6
        Me.pctStdStart.TabStop = False
        '
        'pctStdFinal
        '
        Me.pctStdFinal.Image = CType(resources.GetObject("pctStdFinal.Image"), System.Drawing.Image)
        Me.pctStdFinal.Location = New System.Drawing.Point(554, 476)
        Me.pctStdFinal.Name = "pctStdFinal"
        Me.pctStdFinal.Size = New System.Drawing.Size(1048, 510)
        Me.pctStdFinal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pctStdFinal.TabIndex = 7
        Me.pctStdFinal.TabStop = False
        '
        'prtLabel
        '
        '
        'tmrPollCTS
        '
        '
        'fMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1000, 643)
        Me.Controls.Add(Me.pctStdFinal)
        Me.Controls.Add(Me.pctStdStart)
        Me.Controls.Add(Me.pctLockDrawer)
        Me.Controls.Add(Me.pctGreenLEDon)
        Me.Controls.Add(Me.txtPN)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "fMain"
        Me.Text = "Form1"
        CType(Me.pctGreenLEDon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctLockDrawer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctStdStart, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pctStdFinal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtPN As System.Windows.Forms.TextBox
    Friend WithEvents pctGreenLEDon As System.Windows.Forms.PictureBox
    Friend WithEvents pctLockDrawer As System.Windows.Forms.PictureBox
    Friend WithEvents pctStdStart As System.Windows.Forms.PictureBox
    Friend WithEvents pctStdFinal As System.Windows.Forms.PictureBox
    Friend WithEvents m_Comm As System.IO.Ports.SerialPort
    Friend WithEvents prtLabel As System.Drawing.Printing.PrintDocument
    Friend WithEvents tmrPollCTS As System.Windows.Forms.Timer

End Class
