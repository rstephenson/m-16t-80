﻿Public Class userInput
    'v1.2.3.2
    Public Function confirm_drawerpartnumber(ByVal dwrpn As String) As Boolean
        Dim validpn As Boolean = False

        If (dwrpn.Length > 10) Then
            'QM-000737-1
            If ((dwrpn.Substring(0, 3) = "QM-") And _
                (IsNumeric(dwrpn.Substring(3, 6)) And _
                (dwrpn.Substring(9, 1) = "-"))) Then validpn = True
            'QM1-000737-1
            If ((dwrpn.Substring(0, 4) = "QM1-") And _
                (IsNumeric(dwrpn.Substring(4, 6)) And _
                 (dwrpn.Substring(10, 1) = "-"))) Then validpn = True
        End If

        If (dwrpn.Contains(Chr(32).ToString) Or _
            dwrpn.Contains(",") Or _
            dwrpn.Contains("<") Or _
            dwrpn.Contains(".") Or _
            dwrpn.Contains(">") Or _
            dwrpn.Contains("/") Or _
            dwrpn.Contains("?") Or _
            dwrpn.Contains(";") Or _
            dwrpn.Contains(":") Or _
            dwrpn.Contains("'") Or _
            dwrpn.Contains(Chr(34).ToString) Or _
            dwrpn.Contains("[") Or _
            dwrpn.Contains("{") Or _
            dwrpn.Contains("]") Or _
            dwrpn.Contains("}") Or _
            dwrpn.Contains("\") Or _
            dwrpn.Contains("|") Or _
            dwrpn.Contains("`") Or _
            dwrpn.Contains("~") Or _
            dwrpn.Contains("!") Or _
            dwrpn.Contains("@") Or _
            dwrpn.Contains("#") Or _
            dwrpn.Contains("$") Or _
            dwrpn.Contains("%") Or _
            dwrpn.Contains("^") Or _
            dwrpn.Contains("&") Or _
            dwrpn.Contains("*") Or _
            dwrpn.Contains("(") Or _
            dwrpn.Contains(")") Or _
            dwrpn.Contains("_") Or _
            dwrpn.Contains("=") Or _
            dwrpn.Contains("+")) Then
            validpn = False
        End If
        Return validpn
    End Function
End Class
